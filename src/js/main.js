"use strict";

// set initial vars
var score = 0;
// declare all button level values referenced
// to be able to change them from within functions
var clickMeBtnLvl = {r:0};
var autoclickerv1BtnLvl = {r:0};
var autoclickerv2BtnLvl = {r:0};
var pointereiellionBtnLvl = {r:0};
var upgrade4BtnLvl = {r:0};
var upgrade5BtnLvl = {r:0};
var upgrade6BtnLvl = {r:0};
var upgrade7BtnLvl = {r:0};
var upgrade8BtnLvl = {r:0};
var upgrade9BtnLvl = {r:0};
var upgrade10BtnLvl = {r:0};
var upgrade11BtnLvl = {r:0};
var upgrade12BtnLvl = {r:0};
var upgrade13BtnLvl = {r:0};
var upgrade14BtnLvl = {r:0};
var upgrade15BtnLvl = {r:0};
var upgrade16BtnLvl = {r:0};
var upgrade17BtnLvl = {r:0};
var upgrade18BtnLvl = {r:0};
var upgrade19BtnLvl = {r:0};
var upgrade20BtnLvl = {r:0};
var upgrade21BtnLvl = {r:0};
var upgrade22BtnLvl = {r:0};
var upgrade23BtnLvl = {r:0};
var upgrade24BtnLvl = {r:0};
var upgrade25BtnLvl = {r:0};
var upgrade26BtnLvl = {r:0};
var upgrade27BtnLvl = {r:0};
var upgrade28BtnLvl = {r:0};
var upgrade29BtnLvl = {r:0};
var upgrade30BtnLvl = {r:0};
var upgrade31BtnLvl = {r:0};
var upgrade32BtnLvl = {r:0};
var upgrade33BtnLvl = {r:0};
var upgrade34BtnLvl = {r:0};
var upgrade35BtnLvl = {r:0};
var upgrade36BtnLvl = {r:0};
var upgrade37BtnLvl = {r:0};
var upgrade38BtnLvl = {r:0};
var upgrade39BtnLvl = {r:0};
var upgrade40BtnLvl = {r:0};
var upgrade41BtnLvl = {r:0};
var upgrade42BtnLvl = {r:0};
var upgrade43BtnLvl = {r:0};
var upgrade44BtnLvl = {r:0};
var upgrade45BtnLvl = {r:0};
var upgrade46BtnLvl = {r:0};
var upgrade47BtnLvl = {r:0};
var upgrade48BtnLvl = {r:0};
var upgrade49BtnLvl = {r:0};
var upgrade50BtnLvl = {r:0};
//
var btnName = 'Click Me!';
const btnUpgrade = 'UPGRADE AVAILABLE!';
var btnCostValue = 0;
var btnPercentValue = 100;
var ppsChangeValue = 0.04;
var ppsPercentValue = 100;
const upgradeInfo = 'CLICK TO UPGRADE';
const upgradeInfoUnavailable = 'INSUFFICIENT POINTS';
var ppsValue = 0;
var timeValue = '00.0';
var playerNameStr = 'Anonymous';
const scoreComment = 'Your glorious ClickPoints empire begins';
var scoreMilestone5 = false;
const scoreMilestone5text = 'The hard days of manual button clicking labour seem endless, but a better future is in sight.';
var scoreMilestone15 = false;
const scoreMilestone15text = 'The investment is returned - you finally turn a profit.';
var scoreMilestone200 = false;
const scoreMilestone200text = 'Your long days of labour to gather the initial 12 points are a fast-fading memory.';
var scoreMilestone100K = false;
const scoreMilestone100Ktext = 'You reach international acclaim as a prominent and incredibly wealthy point collector.';
var scoreMilestone1BN= false;
const scoreMilestone1BNtext = 'Your enormous pile of points is now larger than Everest';
var musicPlaying = false;
var musicFile = new Audio('../media/Rico_Puestel-Roja_Drifts_By.ogg');
const musicOffImg = "../img/volume_off_white_24dp.svg";
const musicOnImg = "../img/volume_up_white_24dp.svg";

// define display elements
const scoreElem = document.querySelector("#score");
const clickMeBtnLevelElem = document.querySelector("#clickMeBtnLevel");
const autoclickerv1BtnLevelElem = document.querySelector("#autoclickerv1BtnLevel");
const autoclickerv2BtnLevelElem = document.querySelector("#autoclickerv2BtnLevel");
const pointereiellionBtnLevelElem = document.querySelector("#pointereiellionBtnLevel");
const upgrade4BtnLevelElem = document.querySelector("#upgrade4BtnLevel");
const upgrade5BtnLevelElem = document.querySelector("#upgrade5BtnLevel");
const upgrade6BtnLevelElem = document.querySelector("#upgrade6BtnLevel");
const upgrade7BtnLevelElem = document.querySelector("#upgrade7BtnLevel");
const upgrade8BtnLevelElem = document.querySelector("#upgrade8BtnLevel");
const upgrade9BtnLevelElem = document.querySelector("#upgrade9BtnLevel");
const upgrade10BtnLevelElem = document.querySelector("#upgrade10BtnLevel");
const upgrade11BtnLevelElem = document.querySelector("#upgrade11BtnLevel");
const upgrade12BtnLevelElem = document.querySelector("#upgrade12BtnLevel");
const upgrade13BtnLevelElem = document.querySelector("#upgrade13BtnLevel");
const upgrade14BtnLevelElem = document.querySelector("#upgrade14BtnLevel");
const upgrade15BtnLevelElem = document.querySelector("#upgrade15BtnLevel");
const upgrade16BtnLevelElem = document.querySelector("#upgrade16BtnLevel");
const upgrade17BtnLevelElem = document.querySelector("#upgrade17BtnLevel");
const upgrade18BtnLevelElem = document.querySelector("#upgrade18BtnLevel");
const upgrade19BtnLevelElem = document.querySelector("#upgrade19BtnLevel");
const upgrade20BtnLevelElem = document.querySelector("#upgrade20BtnLevel");
const upgrade21BtnLevelElem = document.querySelector("#upgrade21BtnLevel");
const upgrade22BtnLevelElem = document.querySelector("#upgrade22BtnLevel");
const upgrade23BtnLevelElem = document.querySelector("#upgrade23BtnLevel");
const upgrade24BtnLevelElem = document.querySelector("#upgrade24BtnLevel");
const upgrade25BtnLevelElem = document.querySelector("#upgrade25BtnLevel");
const upgrade26BtnLevelElem = document.querySelector("#upgrade26BtnLevel");
const upgrade27BtnLevelElem = document.querySelector("#upgrade27BtnLevel");
const upgrade28BtnLevelElem = document.querySelector("#upgrade28BtnLevel");
const upgrade29BtnLevelElem = document.querySelector("#upgrade29BtnLevel");
const upgrade30BtnLevelElem = document.querySelector("#upgrade30BtnLevel");
const upgrade31BtnLevelElem = document.querySelector("#upgrade31BtnLevel");
const upgrade32BtnLevelElem = document.querySelector("#upgrade32BtnLevel");
const upgrade33BtnLevelElem = document.querySelector("#upgrade33BtnLevel");
const upgrade34BtnLevelElem = document.querySelector("#upgrade34BtnLevel");
const upgrade35BtnLevelElem = document.querySelector("#upgrade35BtnLevel");
const upgrade36BtnLevelElem = document.querySelector("#upgrade36BtnLevel");
const upgrade37BtnLevelElem = document.querySelector("#upgrade37BtnLevel");
const upgrade38BtnLevelElem = document.querySelector("#upgrade38BtnLevel");
const upgrade39BtnLevelElem = document.querySelector("#upgrade39BtnLevel");
const upgrade40BtnLevelElem = document.querySelector("#upgrade40BtnLevel");
const upgrade41BtnLevelElem = document.querySelector("#upgrade41BtnLevel");
const upgrade42BtnLevelElem = document.querySelector("#upgrade42BtnLevel");
const upgrade43BtnLevelElem = document.querySelector("#upgrade43BtnLevel");
const upgrade44BtnLevelElem = document.querySelector("#upgrade44BtnLevel");
const upgrade45BtnLevelElem = document.querySelector("#upgrade45BtnLevel");
const upgrade46BtnLevelElem = document.querySelector("#upgrade46BtnLevel");
const upgrade47BtnLevelElem = document.querySelector("#upgrade47BtnLevel");
const upgrade48BtnLevelElem = document.querySelector("#upgrade48BtnLevel");
const upgrade49BtnLevelElem = document.querySelector("#upgrade49BtnLevel");
const upgrade50BtnLevelElem = document.querySelector("#upgrade50BtnLevel");
const btnNameElem = document.querySelector("#btnName");
const btnUpgradeElem = document.querySelector("#btnUpgrade");
const btnCostValueElem = document.querySelector("#btnCostValue");
const btnPercentValueElem = document.querySelector("#btnPercentValue");
const ppsChangeValueElem = document.querySelector("#ppsChangeValue");
const ppsPercentValueElem = document.querySelector("#ppsPercentValue");
const upgradeInfoElem = document.querySelector("#upgradeInfo");
const ppsValueElem = document.querySelector("#ppsValue");
const timeValueElem = document.querySelector("#timeValue");
const scoreCommentElem = document.querySelector("#scoreComment");
const playerNameElem = document.querySelector("#playerName");
const setUserNameElem = document.querySelector("#setUserNameBtn");
const inputUserNameElem = document.querySelector("#inputUsername");

// define button elements
const clickMeBtn = document.querySelector("#clickMeBtn");
const autoclickerv1Btn = document.querySelector("#autoclickerv1Btn");
const autoclickerv2Btn = document.querySelector("#autoclickerv2Btn");
const pointereiellionBtn = document.querySelector("#pointereiellionBtn");
const upgrade4Btn = document.querySelector("#upgrade4Btn");
const upgrade5Btn = document.querySelector("#upgrade5Btn");
const upgrade6Btn = document.querySelector("#upgrade6Btn");
const upgrade7Btn = document.querySelector("#upgrade7Btn");
const upgrade8Btn = document.querySelector("#upgrade8Btn");
const upgrade9Btn = document.querySelector("#upgrade9Btn");
const upgrade10Btn = document.querySelector("#upgrade10Btn");
const upgrade11Btn = document.querySelector("#upgrade11Btn");
const upgrade12Btn = document.querySelector("#upgrade12Btn");
const upgrade13Btn = document.querySelector("#upgrade13Btn");
const upgrade14Btn = document.querySelector("#upgrade14Btn");
const upgrade15Btn = document.querySelector("#upgrade15Btn");
const upgrade16Btn = document.querySelector("#upgrade16Btn");
const upgrade17Btn = document.querySelector("#upgrade17Btn");
const upgrade18Btn = document.querySelector("#upgrade18Btn");
const upgrade19Btn = document.querySelector("#upgrade19Btn");
const upgrade20Btn = document.querySelector("#upgrade20Btn");
const upgrade21Btn = document.querySelector("#upgrade21Btn");
const upgrade22Btn = document.querySelector("#upgrade22Btn");
const upgrade23Btn = document.querySelector("#upgrade23Btn");
const upgrade24Btn = document.querySelector("#upgrade24Btn");
const upgrade25Btn = document.querySelector("#upgrade25Btn");
const upgrade26Btn = document.querySelector("#upgrade26Btn");
const upgrade27Btn = document.querySelector("#upgrade27Btn");
const upgrade28Btn = document.querySelector("#upgrade28Btn");
const upgrade29Btn = document.querySelector("#upgrade29Btn");
const upgrade30Btn = document.querySelector("#upgrade30Btn");
const upgrade31Btn = document.querySelector("#upgrade31Btn");
const upgrade32Btn = document.querySelector("#upgrade32Btn");
const upgrade33Btn = document.querySelector("#upgrade33Btn");
const upgrade34Btn = document.querySelector("#upgrade34Btn");
const upgrade35Btn = document.querySelector("#upgrade35Btn");
const upgrade36Btn = document.querySelector("#upgrade36Btn");
const upgrade37Btn = document.querySelector("#upgrade37Btn");
const upgrade38Btn = document.querySelector("#upgrade38Btn");
const upgrade39Btn = document.querySelector("#upgrade39Btn");
const upgrade40Btn = document.querySelector("#upgrade40Btn");
const upgrade41Btn = document.querySelector("#upgrade41Btn");
const upgrade42Btn = document.querySelector("#upgrade42Btn");
const upgrade43Btn = document.querySelector("#upgrade43Btn");
const upgrade44Btn = document.querySelector("#upgrade44Btn");
const upgrade45Btn = document.querySelector("#upgrade45Btn");
const upgrade46Btn = document.querySelector("#upgrade46Btn");
const upgrade47Btn = document.querySelector("#upgrade47Btn");
const upgrade48Btn = document.querySelector("#upgrade48Btn");
const upgrade49Btn = document.querySelector("#upgrade49Btn");
const upgrade50Btn = document.querySelector("#upgrade50Btn");
const setUserNameBtn = document.querySelector("#setUserNameBtn");
const setMusicBtn = document.querySelector("#music");

// listen for events from buttons and other elements
clickMeBtn.addEventListener("click", doClick.bind(clickMeBtn, 'Click Me!', 0, 0.04, clickMeBtnLvl, clickMeBtnLevelElem));
clickMeBtn.addEventListener("mouseover", showUpdate.bind(clickMeBtn, 'Click Me!', 0, 0.04));
autoclickerv1Btn.addEventListener("click", doUpdate.bind(autoclickerv1Btn, 'Autoclicker v1', 12, 1, autoclickerv1BtnLvl, autoclickerv1BtnLevelElem));
autoclickerv1Btn.addEventListener("mouseover", showUpdate.bind(autoclickerv1Btn, 'Autoclicker v1', 12, 1));
autoclickerv2Btn.addEventListener("click", doUpdate.bind(autoclickerv2Btn, 'Autoclicker v2', 130, 10, autoclickerv2BtnLvl, autoclickerv2BtnLevelElem));
autoclickerv2Btn.addEventListener("mouseover", showUpdate.bind(autoclickerv2Btn, 'Autoclicker v2', 130, 10));
pointereiellionBtn.addEventListener("click", doUpdate.bind(pointereiellionBtn, 'Pointereiellion', 2600, 200, pointereiellionBtnLvl, pointereiellionBtnLevelElem));
pointereiellionBtn.addEventListener("mouseover", showUpdate.bind(pointereiellionBtn, 'Pointereiellion', 2600, 200));
upgrade4Btn.addEventListener("click", doUpdate.bind(upgrade4Btn, 'Upgrade 4', 3159, 243, upgrade4BtnLvl, upgrade4BtnLevelElem));
upgrade4Btn.addEventListener("mouseover", showUpdate.bind(upgrade4Btn, 'Upgrade 4', 3159, 243));
upgrade5Btn.addEventListener("click", doUpdate.bind(upgrade5Btn, 'Upgrade 5', 13312, 1024, upgrade5BtnLvl, upgrade5BtnLevelElem));
upgrade5Btn.addEventListener("mouseover", showUpdate.bind(upgrade5Btn, 'Upgrade 5', 13312, 1024));
upgrade6Btn.addEventListener("click", doUpdate.bind(upgrade6Btn, 'Upgrade 6', 40625, 3125, upgrade6BtnLvl, upgrade6BtnLevelElem));
upgrade6Btn.addEventListener("mouseover", showUpdate.bind(upgrade6Btn, 'Upgrade 6', 40625, 3125));
upgrade7Btn.addEventListener("click", doUpdate.bind(upgrade7Btn, 'Upgrade 7', 101088, 7776, upgrade7BtnLvl, upgrade7BtnLevelElem));
upgrade7Btn.addEventListener("mouseover", showUpdate.bind(upgrade7Btn, 'Upgrade 7', 101088, 7776));
upgrade8Btn.addEventListener("click", doUpdate.bind(upgrade8Btn, 'Upgrade 8', 218491, 16807, upgrade8BtnLvl, upgrade8BtnLevelElem));
upgrade8Btn.addEventListener("mouseover", showUpdate.bind(upgrade8Btn, 'Upgrade 8', 218491, 16807));
upgrade9Btn.addEventListener("click", doUpdate.bind(upgrade9Btn, 'Upgrade 9', 425984, 32768, upgrade9BtnLvl, upgrade9BtnLevelElem));
upgrade9Btn.addEventListener("mouseover", showUpdate.bind(upgrade9Btn, 'Upgrade 9', 425984, 32768));
upgrade10Btn.addEventListener("click", doUpdate.bind(upgrade10Btn, 'Upgrade 10', 767637, 59049, upgrade10BtnLvl, upgrade10BtnLevelElem));
upgrade10Btn.addEventListener("mouseover", showUpdate.bind(upgrade10Btn, 'Upgrade 10', 767637, 59049));
upgrade11Btn.addEventListener("click", doUpdate.bind(upgrade11Btn, 'Upgrade 11', 1300000, 100000, upgrade11BtnLvl, upgrade11BtnLevelElem));
upgrade11Btn.addEventListener("mouseover", showUpdate.bind(upgrade11Btn, 'Upgrade 11', 1300000, 100000));
upgrade12Btn.addEventListener("click", doUpdate.bind(upgrade12Btn, 'Upgrade 12', 2093663, 161051, upgrade12BtnLvl, upgrade12BtnLevelElem));
upgrade12Btn.addEventListener("mouseover", showUpdate.bind(upgrade12Btn, 'Upgrade 12', 2093663, 161051));
upgrade13Btn.addEventListener("click", doUpdate.bind(upgrade13Btn, 'Upgrade 13', 3234816, 248832, upgrade13BtnLvl, upgrade13BtnLevelElem));
upgrade13Btn.addEventListener("mouseover", showUpdate.bind(upgrade13Btn, 'Upgrade 13', 3234816, 248832));
upgrade14Btn.addEventListener("click", doUpdate.bind(upgrade14Btn, 'Upgrade 14', 4826809, 371293, upgrade14BtnLvl, upgrade14BtnLevelElem));
upgrade14Btn.addEventListener("mouseover", showUpdate.bind(upgrade14Btn, 'Upgrade 14', 4826809, 371293));
upgrade15Btn.addEventListener("click", doUpdate.bind(upgrade15Btn, 'Upgrade 15', 6991712, 537824, upgrade15BtnLvl, upgrade15BtnLevelElem));
upgrade15Btn.addEventListener("mouseover", showUpdate.bind(upgrade15Btn, 'Upgrade 15', 6991712, 537824));
upgrade16Btn.addEventListener("click", doUpdate.bind(upgrade16Btn, 'Upgrade 16', 9871875, 759375, upgrade16BtnLvl, upgrade16BtnLevelElem));
upgrade16Btn.addEventListener("mouseover", showUpdate.bind(upgrade16Btn, 'Upgrade 16', 9871875, 759375));
upgrade17Btn.addEventListener("click", doUpdate.bind(upgrade17Btn, 'Upgrade 17', 13631490, 1048576, upgrade17BtnLvl, upgrade17BtnLevelElem));
upgrade17Btn.addEventListener("mouseover", showUpdate.bind(upgrade17Btn, 'Upgrade 17', 13631490, 1048576));
upgrade18Btn.addEventListener("click", doUpdate.bind(upgrade18Btn, 'Upgrade 18', 18458140, 1419857, upgrade18BtnLvl, upgrade18BtnLevelElem));
upgrade18Btn.addEventListener("mouseover", showUpdate.bind(upgrade18Btn, 'Upgrade 18', 18458140, 1419857));
upgrade19Btn.addEventListener("click", doUpdate.bind(upgrade19Btn, 'Upgrade 19', 24564380, 1889568, upgrade19BtnLvl, upgrade19BtnLevelElem));
upgrade19Btn.addEventListener("mouseover", showUpdate.bind(upgrade19Btn, 'Upgrade 19', 24564380, 1889568));
upgrade20Btn.addEventListener("click", doUpdate.bind(upgrade20Btn, 'Upgrade 20', 32189290, 2476099, upgrade20BtnLvl, upgrade20BtnLevelElem));
upgrade20Btn.addEventListener("mouseover", showUpdate.bind(upgrade20Btn, 'Upgrade 20', 32189290, 2476099));
upgrade21Btn.addEventListener("click", doUpdate.bind(upgrade21Btn, 'Upgrade 21', 41600000, 3200000, upgrade21BtnLvl, upgrade21BtnLevelElem));
upgrade21Btn.addEventListener("mouseover", showUpdate.bind(upgrade21Btn, 'Upgrade 21', 41600000, 3200000));
upgrade22Btn.addEventListener("click", doUpdate.bind(upgrade22Btn, 'Upgrade 22', 53093310, 4084101, upgrade22BtnLvl, upgrade22BtnLevelElem));
upgrade22Btn.addEventListener("mouseover", showUpdate.bind(upgrade22Btn, 'Upgrade 22', 53093310, 4084101));
upgrade23Btn.addEventListener("click", doUpdate.bind(upgrade23Btn, 'Upgrade 23', 66997220, 5153632, upgrade23BtnLvl, upgrade23BtnLevelElem));
upgrade23Btn.addEventListener("mouseover", showUpdate.bind(upgrade23Btn, 'Upgrade 23', 66997220, 5153632));
upgrade24Btn.addEventListener("click", doUpdate.bind(upgrade24Btn, 'Upgrade 24', 83672460, 6436343, upgrade24BtnLvl, upgrade24BtnLevelElem));
upgrade24Btn.addEventListener("mouseover", showUpdate.bind(upgrade24Btn, 'Upgrade 24', 83672460, 6436343));
upgrade25Btn.addEventListener("click", doUpdate.bind(upgrade25Btn, 'Upgrade 25', 103514100, 7962624, upgrade25BtnLvl, upgrade25BtnLevelElem));
upgrade25Btn.addEventListener("mouseover", showUpdate.bind(upgrade25Btn, 'Upgrade 25', 103514100, 7962624));
upgrade26Btn.addEventListener("click", doUpdate.bind(upgrade26Btn, 'Upgrade 26', 126953100, 9765625, upgrade26BtnLvl, upgrade26BtnLevelElem));
upgrade26Btn.addEventListener("mouseover", showUpdate.bind(upgrade26Btn, 'Upgrade 26', 126953100, 9765625));
upgrade27Btn.addEventListener("click", doUpdate.bind(upgrade27Btn, 'Upgrade 27', 154457900, 11881380, upgrade27BtnLvl, upgrade27BtnLevelElem));
upgrade27Btn.addEventListener("mouseover", showUpdate.bind(upgrade27Btn, 'Upgrade 27', 154457900, 11881380));
upgrade28Btn.addEventListener("click", doUpdate.bind(upgrade28Btn, 'Upgrade 28', 186535800, 14348910, upgrade28BtnLvl, upgrade28BtnLevelElem));
upgrade28Btn.addEventListener("mouseover", showUpdate.bind(upgrade28Btn, 'Upgrade 28', 186535800, 14348910));
upgrade29Btn.addEventListener("click", doUpdate.bind(upgrade29Btn, 'Upgrade 29', 223734800, 17210370, upgrade29BtnLvl, upgrade29BtnLevelElem));
upgrade29Btn.addEventListener("mouseover", showUpdate.bind(upgrade29Btn, 'Upgrade 29', 223734800, 17210370));
upgrade30Btn.addEventListener("click", doUpdate.bind(upgrade30Btn, 'Upgrade 30', 266644900, 20511150, upgrade30BtnLvl, upgrade30BtnLevelElem));
upgrade30Btn.addEventListener("mouseover", showUpdate.bind(upgrade30Btn, 'Upgrade 30', 266644900, 20511150));
upgrade31Btn.addEventListener("click", doUpdate.bind(upgrade31Btn, 'Upgrade 31', 315900000, 24300000, upgrade31BtnLvl, upgrade31BtnLevelElem));
upgrade31Btn.addEventListener("mouseover", showUpdate.bind(upgrade31Btn, 'Upgrade 31', 315900000, 24300000));
upgrade32Btn.addEventListener("click", doUpdate.bind(upgrade32Btn, 'Upgrade 32', 372179000, 28629150, upgrade32BtnLvl, upgrade32BtnLevelElem));
upgrade32Btn.addEventListener("mouseover", showUpdate.bind(upgrade32Btn, 'Upgrade 32', 372179000, 28629150));
upgrade33Btn.addEventListener("click", doUpdate.bind(upgrade33Btn, 'Upgrade 33', 436207600, 33554430, upgrade33BtnLvl, upgrade33BtnLevelElem));
upgrade33Btn.addEventListener("mouseover", showUpdate.bind(upgrade33Btn, 'Upgrade 33', 436207600, 33554430));
upgrade34Btn.addEventListener("click", doUpdate.bind(upgrade34Btn, 'Upgrade 34', 508760100, 39135390, upgrade34BtnLvl, upgrade34BtnLevelElem));
upgrade34Btn.addEventListener("mouseover", showUpdate.bind(upgrade34Btn, 'Upgrade 34', 508760100, 39135390));
upgrade35Btn.addEventListener("click", doUpdate.bind(upgrade35Btn, 'Upgrade 35', 590660500, 45435420, upgrade35BtnLvl, upgrade35BtnLevelElem));
upgrade35Btn.addEventListener("mouseover", showUpdate.bind(upgrade35Btn, 'Upgrade 35', 590660500, 45435420));
upgrade36Btn.addEventListener("click", doUpdate.bind(upgrade36Btn, 'Upgrade 36', 682784400, 52521880, upgrade36BtnLvl, upgrade36BtnLevelElem));
upgrade36Btn.addEventListener("mouseover", showUpdate.bind(upgrade36Btn, 'Upgrade 36', 682784400, 52521880));
upgrade37Btn.addEventListener("click", doUpdate.bind(upgrade37Btn, 'Upgrade 37', 786060300, 60466180, upgrade37BtnLvl, upgrade37BtnLevelElem));
upgrade37Btn.addEventListener("mouseover", showUpdate.bind(upgrade37Btn, 'Upgrade 37', 786060300, 60466180));
upgrade38Btn.addEventListener("click", doUpdate.bind(upgrade38Btn, 'Upgrade 38', 901471500, 69343960, upgrade38BtnLvl, upgrade38BtnLevelElem));
upgrade38Btn.addEventListener("mouseover", showUpdate.bind(upgrade38Btn, 'Upgrade 38', 901471500, 69343960));
upgrade39Btn.addEventListener("click", doUpdate.bind(upgrade39Btn, 'Upgrade 39', 1030057000, 79235170, upgrade39BtnLvl, upgrade39BtnLevelElem));
upgrade39Btn.addEventListener("mouseover", showUpdate.bind(upgrade39Btn, 'Upgrade 39', 1030057000, 79235170));
upgrade40Btn.addEventListener("click", doUpdate.bind(upgrade40Btn, 'Upgrade 40', 1172915000, 90224200, upgrade40BtnLvl, upgrade40BtnLevelElem));
upgrade40Btn.addEventListener("mouseover", showUpdate.bind(upgrade40Btn, 'Upgrade 40', 1172915000, 90224200));
upgrade41Btn.addEventListener("click", doUpdate.bind(upgrade41Btn, 'Upgrade 41', 1331200000, 102400000, upgrade41BtnLvl, upgrade41BtnLevelElem));
upgrade41Btn.addEventListener("mouseover", showUpdate.bind(upgrade41Btn, 'Upgrade 41', 1331200000, 102400000));
upgrade42Btn.addEventListener("click", doUpdate.bind(upgrade42Btn, 'Upgrade 42', 1506131000, 115856200, upgrade42BtnLvl, upgrade42BtnLevelElem));
upgrade42Btn.addEventListener("mouseover", showUpdate.bind(upgrade42Btn, 'Upgrade 42', 1506131000, 115856200));
upgrade43Btn.addEventListener("click", doUpdate.bind(upgrade43Btn, 'Upgrade 43', 1698986000, 130691200, upgrade43BtnLvl, upgrade43BtnLevelElem));
upgrade43Btn.addEventListener("mouseover", showUpdate.bind(upgrade43Btn, 'Upgrade 43', 1698986000, 130691200));
upgrade44Btn.addEventListener("click", doUpdate.bind(upgrade44Btn, 'Upgrade 44', 1911110000, 147008400, upgrade44BtnLvl, upgrade44BtnLevelElem));
upgrade44Btn.addEventListener("mouseover", showUpdate.bind(upgrade44Btn, 'Upgrade 44', 1911110000, 147008400));
upgrade45Btn.addEventListener("click", doUpdate.bind(upgrade45Btn, 'Upgrade 45', 2143911000, 164916200, upgrade45BtnLvl, upgrade45BtnLevelElem));
upgrade45Btn.addEventListener("mouseover", showUpdate.bind(upgrade45Btn, 'Upgrade 45', 2143911000, 164916200));
upgrade46Btn.addEventListener("click", doUpdate.bind(upgrade46Btn, 'Upgrade 46', 2398866000, 184528100, upgrade46BtnLvl, upgrade46BtnLevelElem));
upgrade46Btn.addEventListener("mouseover", showUpdate.bind(upgrade46Btn, 'Upgrade 46', 2398866000, 184528100));
upgrade47Btn.addEventListener("click", doUpdate.bind(upgrade47Btn, 'Upgrade 47', 2677519000, 205963000, upgrade47BtnLvl, upgrade47BtnLevelElem));
upgrade47Btn.addEventListener("mouseover", showUpdate.bind(upgrade47Btn, 'Upgrade 47', 2677519000, 205963000));
upgrade48Btn.addEventListener("click", doUpdate.bind(upgrade48Btn, 'Upgrade 48', 2981485000, 229345000, upgrade48BtnLvl, upgrade48BtnLevelElem));
upgrade48Btn.addEventListener("mouseover", showUpdate.bind(upgrade48Btn, 'Upgrade 48', 2981485000, 229345000));
upgrade49Btn.addEventListener("click", doUpdate.bind(upgrade49Btn, 'Upgrade 49', 3312452000, 254804000, upgrade49BtnLvl, upgrade49BtnLevelElem));
upgrade49Btn.addEventListener("mouseover", showUpdate.bind(upgrade49Btn, 'Upgrade 49', 3312452000, 254804000));
upgrade50Btn.addEventListener("click", doUpdate.bind(upgrade50Btn, 'Upgrade 50', 1454183000000, 282475300, upgrade50BtnLvl, upgrade50BtnLevelElem));
upgrade50Btn.addEventListener("mouseover", showUpdate.bind(upgrade50Btn, 'Upgrade 50', 1454183000000, 282475300));
setUserNameBtn.addEventListener("click", setUserName);
setMusicBtn.addEventListener("click", setMusic);

// display if upgrade is possible or not
function showUpdate(name, cost, ppsAdd) {
    btnNameElem.innerText = name; //set name of button / upgrade
    btnCostValueElem.innerText = cost; // display cost of upgrade
    ppsChangeValueElem.innerText = ppsAdd; // display possible change to pps
    if (score >= cost) {
        // display available upgrade
        btnUpgradeElem.innerText = btnUpgrade;
        upgradeInfoElem.innerText = upgradeInfo;
        btnUpgradeElem.style.color = '#bf237c';
    } else { // inform about insufficient points
        btnUpgradeElem.innerText = upgradeInfoUnavailable;
        upgradeInfoElem.innerText = upgradeInfoUnavailable;
        btnUpgradeElem.style.color = '#808080';
    }
}

// increment score via default button
function doClick(name, cost, ppsAdd, btnLvl, btnLvlElem) {
    btnNameElem.innerText = name; //set name of button / upgrade
    btnCostValueElem.innerText = cost; // display cost of upgrade
    ppsChangeValueElem.innerText = ppsAdd; // display possible change to pps
    btnLvl.r++; // increment button level and display it with desired leading zeros
    if (btnLvl.r < 10) {
        btnLvlElem.textContent = `00${btnLvl.r}`;
    }
    else if ((btnLvl.r >= 10) && (btnLvl.r < 100)) {
        btnLvlElem.textContent = `0${btnLvl.r}`;
    } else {
        btnLvlElem.textContent = btnLvl.r;
    }
    // always display available upgrade for default button
    btnUpgradeElem.innerText = btnUpgrade;
    upgradeInfoElem.innerText = upgradeInfo;
    btnUpgradeElem.style.color = '#bf237c';
    // default button gives direct points
    score += ppsAdd;
    scoreElem.innerText = Math.round(score);
    // set new pps value and display is
    ppsValue = ppsValue += ppsAdd;
    ppsValueElem.innerText = ppsValue.toFixed(1);
    // change button color if upgrade is available
    checkPossibleUpgrade();
}

// buy PPS upgrade
function doUpdate(name, cost, ppsAdd, btnLvl, btnLvlElem) {
    btnNameElem.innerText = name; //set name of upgrade
    btnCostValueElem.innerText = cost; // display cost of upgrade
    ppsChangeValueElem.innerText = ppsAdd; // display possible change to pps
    // pay for upgrade with sufficient points score
    if (score >= cost) {
        // display available upgrade
        btnUpgradeElem.innerText = btnUpgrade;
        upgradeInfoElem.innerText = upgradeInfo;
        btnUpgradeElem.style.color = '#bf237c';
        // pay with points from score
        score = score -= cost;
        scoreElem.innerText = Math.round(score);
        // set new pps value and display it
        ppsValue = ppsValue += ppsAdd;
        ppsValueElem.innerText = ppsValue.toFixed(1);
        btnLvl.r++; // increment button level and display it with desired leading zeros
        // the value is referenced to the global variable
        if (btnLvl.r < 10) {
            btnLvlElem.textContent = `00${btnLvl.r}`;
        }
        else if ((btnLvl.r >= 10) && (btnLvl.r < 100)) {
            btnLvlElem.textContent = `0${btnLvl.r}`;
        } else {
            btnLvlElem.textContent = btnLvl.r;
        }
        // change button color if upgrade is available
        checkPossibleUpgrade();
    } else { // inform about insufficient points
        btnUpgradeElem.innerText = upgradeInfoUnavailable;
        upgradeInfoElem.innerText = upgradeInfoUnavailable;
        btnUpgradeElem.style.color = '#808080';
    }
}

/**
 * Sanitize and encode all HTML in a user-submitted string
 * https://portswigger.net/web-security/cross-site-scripting/preventing
 * @param  {String} str  The user-submitted string
 * @return {String} str  The sanitized string
 */
 var sanitizeHTML = function (str) {
	return str.replace(/[^\w. ]/gi, function (c) {
		return '&#' + c.charCodeAt(0) + ';';
	});
};

function setUserName() {
    // get input from text field
    const rawInput = inputUserNameElem.value;
    // sanitize input
    var cleanInput = sanitizeHTML(rawInput);
    // ensure the string is always only 16 characters long
    cleanInput = cleanInput.slice(0,16);
    // use default name if no text was entered
    if (cleanInput == '') {
        playerNameStr = 'Anonymous';
    }
    else{
        playerNameStr = cleanInput;
    }
    playerNameElem.textContent = playerNameStr; // update the displayed username
    inputUserNameElem.value = ''; // reset input field
    // disable the button and input field to prevent spam of username change
    setUserNameBtn.disabled = true;
    inputUserNameElem.disabled = true;
    // set the input field value which allows it to be hidden in the next step
    inputUserNameElem.value = '.';
    // hide the elements which were used for the username change
    setUserNameElem.style.color = 'transparent';
    setUserNameElem.style.backgroundColor = 'transparent';
    setUserNameElem.style.borderColor = 'transparent';
    inputUserNameElem.style.color = 'transparent';
    inputUserNameElem.style.backgroundColor = 'transparent';
    inputUserNameElem.style.borderColor = 'transparent';
}

function setMusic() {
    // start to play audio
    if (musicPlaying == false) {
        musicFile.play();
        musicFile.loop = true;
        setMusicBtn.src = musicOffImg; // load image into cache
        setMusicBtn.src = musicOnImg;
    }
    // mute or unmute the audio depending on the current state
    if ((musicFile.muted == false) && (musicPlaying == true)) {
        musicFile.muted = true;
        setMusicBtn.src = musicOffImg;
    } else {
        musicFile.muted = false;
        setMusicBtn.src = musicOnImg;
    }
    musicPlaying = true;
}

function checkPossibleUpgrade() {
    // declare methods to process elements
    function showAvailable(buttonElem) {
        buttonElem.style.color = '#ffffff'; //white
        buttonElem.style.backgroundColor = '#2f4f4f'; //darkslategrey
    }
    function showUnavailable(buttonElem) {
        buttonElem.style.color = '#808080'; //gray
        buttonElem.style.backgroundColor = 'transparent';
    }

    // if an upgrade is available change the button color
    if (score >= 12) {
        showAvailable(autoclickerv1Btn);
    } else {
        showUnavailable(autoclickerv1Btn);
    }
    if (score >= 130) {
        showAvailable(autoclickerv2Btn);
    } else {
        showUnavailable(autoclickerv2Btn);
    }
    if (score >= 2600) {
        showAvailable(pointereiellionBtn);
    } else {
        showUnavailable(pointereiellionBtn);
    }
    if (score >= 3159) {
        showAvailable(upgrade4Btn);
    } else {
        showUnavailable(upgrade4Btn);
    }
    if (score >= 13312) {
        showAvailable(upgrade5Btn);
    } else {
        showUnavailable(upgrade5Btn);
    }
    if (score >= 40625) {
        showAvailable(upgrade6Btn);
    } else {
        showUnavailable(upgrade6Btn);
    }
    if (score >= 101088) {
        showAvailable(upgrade7Btn);
    } else {
        showUnavailable(upgrade7Btn);
    }
    if (score >= 218491) {
        showAvailable(upgrade8Btn);
    } else {
        showUnavailable(upgrade8Btn);
    }
    if (score >= 425984) {
        showAvailable(upgrade9Btn);
    } else {
        showUnavailable(upgrade9Btn);
    }
    if (score >= 767637) {
        showAvailable(upgrade10Btn);
    } else {
        showUnavailable(upgrade10Btn);
    }
    if (score >= 1300000) {
        showAvailable(upgrade11Btn);
    } else {
        showUnavailable(upgrade11Btn);
    }
    if (score >= 2093663) {
        showAvailable(upgrade12Btn);
    } else {
        showUnavailable(upgrade12Btn);
    }
    if (score >= 3234816) {
        showAvailable(upgrade13Btn);
    } else {
        showUnavailable(upgrade13Btn);
    }
    if (score >= 4826809) {
        showAvailable(upgrade14Btn);
    } else {
        showUnavailable(upgrade14Btn);
    }
    if (score >= 6991712) {
        showAvailable(upgrade15Btn);
    } else {
        showUnavailable(upgrade15Btn);
    }
    if (score >= 9871875) {
        showAvailable(upgrade16Btn);
    } else {
        showUnavailable(upgrade16Btn);
    }
    if (score >= 13631490) {
        showAvailable(upgrade17Btn);
    } else {
        showUnavailable(upgrade17Btn);
    }
    if (score >= 18458140) {
        showAvailable(upgrade18Btn);
    } else {
        showUnavailable(upgrade18Btn);
    }
    if (score >= 24564380) {
        showAvailable(upgrade19Btn);
    } else {
        showUnavailable(upgrade19Btn);
    }
    if (score >= 32189290) {
        showAvailable(upgrade20Btn);
    } else {
        showUnavailable(upgrade20Btn);
    }
    if (score >= 41600000) {
        showAvailable(upgrade21Btn);
    } else {
        showUnavailable(upgrade21Btn);
    }
    if (score >= 53093310) {
        showAvailable(upgrade22Btn);
    } else {
        showUnavailable(upgrade22Btn);
    }
    if (score >= 66997220) {
        showAvailable(upgrade23Btn);
    } else {
        showUnavailable(upgrade23Btn);
    }
    if (score >= 83672460) {
        showAvailable(upgrade24Btn);
    } else {
        showUnavailable(upgrade24Btn);
    }
    if (score >= 103514100) {
        showAvailable(upgrade25Btn);
    } else {
        showUnavailable(upgrade25Btn);
    }
    if (score >= 126953100) {
        showAvailable(upgrade26Btn);
    } else {
        showUnavailable(upgrade26Btn);
    }
    if (score >= 154457900) {
        showAvailable(upgrade27Btn);
    } else {
        showUnavailable(upgrade27Btn);
    }
    if (score >= 186535800) {
        showAvailable(upgrade28Btn);
    } else {
        showUnavailable(upgrade28Btn);
    }
    if (score >= 223734800) {
        showAvailable(upgrade29Btn);
    } else {
        showUnavailable(upgrade29Btn);
    }
    if (score >= 266644900) {
        showAvailable(upgrade30Btn);
    } else {
        showUnavailable(upgrade30Btn);
    }
    if (score >= 315900000) {
        showAvailable(upgrade31Btn);
    } else {
        showUnavailable(upgrade31Btn);
    }
    if (score >= 372179000) {
        showAvailable(upgrade32Btn);
    } else {
        showUnavailable(upgrade32Btn);
    }
    if (score >= 436207600) {
        showAvailable(upgrade33Btn);
    } else {
        showUnavailable(upgrade33Btn);
    }
    if (score >= 508760100) {
        showAvailable(upgrade34Btn);
    } else {
        showUnavailable(upgrade34Btn);
    }
    if (score >= 590660500) {
        showAvailable(upgrade35Btn);
    } else {
        showUnavailable(upgrade35Btn);
    }
    if (score >= 682784400) {
        showAvailable(upgrade36Btn);
    } else {
        showUnavailable(upgrade36Btn);
    }
    if (score >= 786060300) {
        showAvailable(upgrade37Btn);
    } else {
        showUnavailable(upgrade37Btn);
    }
    if (score >= 901471500) {
        showAvailable(upgrade38Btn);
    } else {
        showUnavailable(upgrade38Btn);
    }
    if (score >= 1030057000) {
        showAvailable(upgrade39Btn);
    } else {
        showUnavailable(upgrade39Btn);
    }
    if (score >= 1172915000) {
        showAvailable(upgrade40Btn);
    } else {
        showUnavailable(upgrade40Btn);
    }
    if (score >= 1331200000) {
        showAvailable(upgrade41Btn);
    } else {
        showUnavailable(upgrade41Btn);
    }
    if (score >= 1506131000) {
        showAvailable(upgrade42Btn);
    } else {
        showUnavailable(upgrade42Btn);
    }
    if (score >= 1698986000) {
        showAvailable(upgrade43Btn);
    } else {
        showUnavailable(upgrade43Btn);
    }
    if (score >= 1911110000) {
        showAvailable(upgrade44Btn);
    } else {
        showUnavailable(upgrade44Btn);
    }
    if (score >= 2143911000) {
        showAvailable(upgrade45Btn);
    } else {
        showUnavailable(upgrade45Btn);
    }
    if (score >= 2398866000) {
        showAvailable(upgrade46Btn);
    } else {
        showUnavailable(upgrade46Btn);
    }
    if (score >= 2677519000) {
        showAvailable(upgrade47Btn);
    } else {
        showUnavailable(upgrade47Btn);
    }
    if (score >= 2981485000) {
        showAvailable(upgrade48Btn);
    } else {
        showUnavailable(upgrade48Btn);
    }
    if (score >= 3312452000) {
        showAvailable(upgrade49Btn);
    } else {
        showUnavailable(upgrade49Btn);
    }
    if (score >= 1454183000000) {
        showAvailable(upgrade50Btn);
    } else {
        showUnavailable(upgrade50Btn);
    }
}

function setScoreComment () {
    // mark reached score milestones
    if (score >= 5) {
        scoreMilestone5 = true;
    }
    if (score >= 15) {
        scoreMilestone15 = true;
    }
    if (score >= 200) {
        scoreMilestone200 = true;
    }
    if (score >= 100000) {
        scoreMilestone100K = true;
    }
    if (score >= 1000000000000) {
        scoreMilestone1BN = true;
    }

    // display a different text based on the maximum achieved score
    if (scoreMilestone1BN === true) {
        scoreCommentElem.innerText = scoreMilestone1BNtext;
    } else if (scoreMilestone100K === true) {
        scoreCommentElem.innerText = scoreMilestone100Ktext;
    } else if (scoreMilestone200 === true) {
        scoreCommentElem.innerText = scoreMilestone200text;
    } else if (scoreMilestone15 === true) {
        scoreCommentElem.innerText = scoreMilestone15text;
    } else if (scoreMilestone5 === true) {
        scoreCommentElem.innerText = scoreMilestone5text;
    } else {
        scoreCommentElem.innerText = scoreComment;
    }
}

// execute in a loop every 500 milliseconds
setInterval(function() {
    // calculate new score based on current points per second
    score = score += ppsValue/2;
    scoreElem.innerText = Math.round(score);
    // scoreElem.innerText = score.toExponential(4);
    // change button color if upgrade is available
    checkPossibleUpgrade();
    // display a different text based on the maximum achieved score
    setScoreComment();
}, 500);